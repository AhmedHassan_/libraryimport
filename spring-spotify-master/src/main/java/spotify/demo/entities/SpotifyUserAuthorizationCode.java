package spotify.demo.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class SpotifyUserAuthorizationCode {
    private String username;
    private String code;
    private String accessToken;
    private String tokenType;
    private String refreshToken;
    private String state;
    private String scope;
}