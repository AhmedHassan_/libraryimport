package spotify.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationAccessDTO {
	
	private String spotifyAuthorizationRedirectURL;
	private String authorizeResponseType;
	private String authorizationScope;
	private String authorizeUrl;
	private String scope;
	private String responseType;
	private String redirectUri;
	private String clientSecret;
	private String clientId;
	private String url;
	private String msg;
	

}
