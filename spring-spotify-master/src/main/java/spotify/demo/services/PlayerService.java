package spotify.demo.services;


import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import spotify.demo.configs.SpotifyConnectionConfig;
import spotify.demo.services.utils.RestCallUtils;

import java.util.Arrays;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static spotify.demo.services.AuthorizationService.callAction;
import static spotify.demo.services.utils.RestCallUtils.*;

@Component
public class PlayerService {
	
	
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    SpotifyConnectionConfig spotifyConnectionConfig;
    

    public JSONObject getRecentlyPlayed(String token) {
        String createIndexUrl = spotifyConnectionConfig.getPlayerUrlRecentlyPlayed();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("Authorization", token);

        ResponseEntity<JSONObject> responseEntity = callAction(restTemplate, "getRecentlyPlayed", createIndexUrl, GET,
                new HttpEntity<>(null, requestHeaders), JSONObject.class, null);

        checkResponseCodeExpected(responseEntity, Arrays.asList(NO_CONTENT, OK), "getRecentlyPlayed");

        return responseEntity.getBody();
    }
    
    
    
    public JSONObject getPlayList(String token) {
        String createIndexUrl = spotifyConnectionConfig.getGetPlayListsUrl();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("Authorization", token);

        ResponseEntity<JSONObject> responseEntity = callAction(restTemplate, "getPlayLists", createIndexUrl, GET,
                new HttpEntity<>(null, requestHeaders), JSONObject.class, null);

        checkResponseCodeExpected(responseEntity, Arrays.asList(NO_CONTENT, OK), "getPlayLists");

        return responseEntity.getBody();
    }
    
    
    
    
    public JSONObject getUserPlayList(String token,String user_id) {
    	
        String createIndexUrl = "https://api.spotify.com/v1/users/"+user_id+"/playlists";

        System.out.println(" getUserPlayListURL  : " + createIndexUrl);
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("Authorization", token);
        System.out.println("Req Header :"+requestHeaders);
        ResponseEntity<JSONObject> responseEntity = callAction(restTemplate, "playlists", createIndexUrl, GET,
                new HttpEntity<>(requestHeaders), JSONObject.class, null);

      checkResponseCodeExpected(responseEntity, Arrays.asList(NO_CONTENT, OK), "playlists");

        return responseEntity.getBody();
    }
    
    
    
    public JSONObject getPlayListById(String token,String playlist_id) {
    	
        String createIndexUrl = spotifyConnectionConfig.getPlayLists() + playlist_id;

        System.out.println(" getUserPlayListURL  : " + createIndexUrl);
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.set("Authorization", token);
        System.out.println("Req Header :"+requestHeaders);
        ResponseEntity<JSONObject> responseEntity = callAction(restTemplate, "find playlist by id ", createIndexUrl, GET,
                new HttpEntity<>(requestHeaders), JSONObject.class, null);

        checkResponseCodeExpected(responseEntity, Arrays.asList(NO_CONTENT, OK), "playlist");

        return responseEntity.getBody();
    }
    
    
}