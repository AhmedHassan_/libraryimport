package spotify.demo.services;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import java.util.*;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import lombok.extern.slf4j.Slf4j;
import spotify.demo.configs.SpotifyConnectionConfig;
import static spotify.demo.services.utils.RestCallUtils.*;

@Component
@Slf4j
public class AuthorizationService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    SpotifyConnectionConfig spotifyConnectionConfig;
    
 
    /**
     * redirect to the thridparty to login.
     */
    public String grantApplicationAccess() {
    	// private String authorizeUrl = "https://accounts.spotify.com/authorize/";
        String createIndexUrl = spotifyConnectionConfig.getAuthorizeUrl();
        
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("scope", spotifyConnectionConfig.getAuthorizationScope());
        queryParams.put("response_type", spotifyConnectionConfig.getAuthorizeResponseType());
        queryParams.put("redirect_uri", spotifyConnectionConfig.getSpotifyAuthorizationRedirectURL());
        queryParams.put("client_id", spotifyConnectionConfig.getClientId());
        
        
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        
        
        ResponseEntity<String> responseEntity = callAction(restTemplate, "authorize", createIndexUrl, GET,
                new HttpEntity<>(null, requestHeaders), String.class, queryParams);
        
        checkResponseCodeExpectedString(responseEntity, Arrays.asList(CREATED, OK), "authorize");
        
        System.out.println("code : "+ responseEntity.getBody().toString());

        return responseEntity.getBody();
    }

    
    /**
     * Get token for user who has authorized us to interact with spotify to fetch data on his behalf
     */
    public JSONObject getToken(String code) {
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
        
        String createIndexUrl = spotifyConnectionConfig.getTokenUrl();
        
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.set("Content-Type", APPLICATION_FORM_URLENCODED_VALUE);
        requestHeaders.set("Authorization", "Basic " + Base64.getEncoder().encodeToString(
                (spotifyConnectionConfig.getClientId() + ":" + spotifyConnectionConfig.getClientSecret()).getBytes()
        ));
        
        requestBody.put("grant_type", Arrays.asList("authorization_code"));
        requestBody.put("code", Arrays.asList(code));
        requestBody.put("redirect_uri", Arrays.asList(spotifyConnectionConfig.getSpotifyAuthorizationRedirectURL()));

        ResponseEntity<JSONObject> responseEntity = callAction(restTemplate, "getToken", createIndexUrl, POST,
                new HttpEntity<>(requestBody, requestHeaders), JSONObject.class, null);
        
        checkResponseCodeExpected(responseEntity, Arrays.asList(NO_CONTENT, OK), "getToken");
        
        System.out.println("get token : "+ responseEntity.getBody().toString());
        return responseEntity.getBody();
    }
    

    
    
    
    protected static <T> ResponseEntity<T> callAction(RestTemplate restTemplate, String message, String url, HttpMethod method, HttpEntity<?> entity, Class<T> responseClass, Map<String, String> queryParameters) {
        ResponseEntity<T> responseEntity;
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);
            if (queryParameters != null) {
                queryParameters.keySet().forEach(key -> builder.queryParam(key, queryParameters.get(key)));
            }
            responseEntity = restTemplate.exchange(builder.build(false).toUri(), method, entity, responseClass);
        } catch (RestClientResponseException e) {
            log.error("Error while making the " + message + " rest call. Message:{}", e.getResponseBodyAsString());
            throw new RuntimeException("Error while making the " + message + " rest call. Message:" + e.getResponseBodyAsString());
        }
        
        System.out.println("response entity :" + responseEntity);
        
        return responseEntity;
    }


}
