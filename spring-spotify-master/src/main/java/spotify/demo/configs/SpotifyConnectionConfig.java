package spotify.demo.configs;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import javax.validation.constraints.NotEmpty;


@Component 
@Data 
@JsonIgnoreProperties(ignoreUnknown = true) 
@PropertySource("classpath:application.properties")
public class SpotifyConnectionConfig {

    @Value("${spotify.client-id}")
    @NotEmpty
    private String clientId;

    @Value("${spotify.client-secret}")
    @NotEmpty
    private String clientSecret;

    @Value("${spotify.authorization.redirectURL}")
    @NotEmpty
    private String spotifyAuthorizationRedirectURL;

    private String authorizeUrl = "https://accounts.spotify.com/authorize/";

    private String userInfoUri = "https://api.spotify.com/v1/me";    
    
    private String authorizeResponseType = "code";  

    //private String authorizationScope = "user-library-read user-read-private playlist-read-private playlist-modify-public playlist-modify-private user-library-read user-read-recently-played streaming app-remote-control user-read-email user-read-playback-position user-library-read";

    private String authorizationScope = "ugc-image-upload user-read-playback-state user-modify-playback-state user-read-currently-playing streaming app-remote-control user-read-email user-read-private playlist-read-collaborative playlist-modify-public playlist-read-private playlist-modify-private user-library-modify user-library-read user-top-read user-read-playback-position user-read-recently-played user-follow-read user-follow-modify";
    
    private String tokenUrl = "https://accounts.spotify.com/api/token";

    private String playerUrlRecentlyPlayed = "https://api.spotify.com/v1/me/player/recently-played";

    private String albumUrl = "https://api.spotify.com/v1/albums/";
   
    private String getPlayListsUrl = "https://api.spotify.com/v1/me/playlists";
    
    private String getPlayListByUserId = "https://api.spotify.com/v1/users/playlists/";
    
    private String playLists = "https://api.spotify.com/v1/playlists/";
    
}	
