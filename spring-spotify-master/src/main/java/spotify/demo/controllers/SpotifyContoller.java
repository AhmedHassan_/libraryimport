package spotify.demo.controllers;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import spotify.demo.entities.SpotifyUserAuthorizationCode;
import spotify.demo.services.AuthorizationService;
import spotify.demo.services.PlayerService;

@RestController
@RequestMapping("/v1/")
public class SpotifyContoller {

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private PlayerService playerService;

	private SpotifyUserAuthorizationCode spotifyUserAuthorizationCode = new SpotifyUserAuthorizationCode();

	String authCode;

	@GetMapping("/authorize")
	public String authorize() {
		return authorizationService.grantApplicationAccess();
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/token")
	public JSONObject getToken() {
		JSONObject response = new JSONObject();
		if (spotifyUserAuthorizationCode.getCode() == null || spotifyUserAuthorizationCode.getCode().isEmpty()) {
			response.put("Error", "Application not authorized yet on user's behalf to access his data");
			return response;
		}
		JSONObject result = authorizationService.getToken(spotifyUserAuthorizationCode.getCode());
		spotifyUserAuthorizationCode.setAccessToken((String) result.get("access_token"));
		spotifyUserAuthorizationCode.setRefreshToken((String) result.get("refresh_token"));
		spotifyUserAuthorizationCode.setTokenType((String) result.get("token_type"));
		spotifyUserAuthorizationCode.setScope((String) result.get("scope"));
		
		System.out.println("scope : "+ result.get("scope"));
		System.out.println("into get token api : " + result.toString());
		return result;
	}

	@RequestMapping("/responseFromSpotify")
//	@GetMapping("/responseFromSpotify")
	public String authResponse(@RequestParam(required = false) String code,
			@RequestParam(required = false) String state, @RequestParam(required = false) String error) {
		if (error != null) {
			return state;
		}
		spotifyUserAuthorizationCode.setCode(code);
		spotifyUserAuthorizationCode.setUsername("user-" + Thread.currentThread().getName());
		spotifyUserAuthorizationCode.setState(Thread.currentThread().getState().toString());
		System.out.println("code : " + code);

		return code;
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/recentlyPlayed")
	public JSONObject getRecentPlayedTracks() {
		JSONObject response = new JSONObject();
		if (spotifyUserAuthorizationCode.getAccessToken() == null
				|| spotifyUserAuthorizationCode.getAccessToken().isEmpty()) {
			response.put("Error", "UserAccessToken not fetched yet");
			return response;
		}
		JSONObject result = playerService.getRecentlyPlayed(
				spotifyUserAuthorizationCode.getTokenType() + " " + spotifyUserAuthorizationCode.getAccessToken());
		return result;
	}

	@SuppressWarnings("unchecked")
	@GetMapping("/getplaylist")
	public JSONObject getPlayList() {
		JSONObject response = new JSONObject();
		if (spotifyUserAuthorizationCode.getAccessToken() == null
				|| spotifyUserAuthorizationCode.getAccessToken().isEmpty()) {
			response.put("Error", "UserAccessToken not fetched yet");
			return response;
		}
		JSONObject result = playerService.getPlayList(
				spotifyUserAuthorizationCode.getTokenType() + " " + spotifyUserAuthorizationCode.getAccessToken());
		return result;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@GetMapping("/getUserplaylist/{user_id}")
	public JSONObject getPlayListByUserId(@PathVariable String user_id) {
		
		System.out.println("into getPlayListByUserId ");
		JSONObject response = new JSONObject();
		if (spotifyUserAuthorizationCode.getAccessToken() == null
				|| spotifyUserAuthorizationCode.getAccessToken().isEmpty()) {
			response.put("Error", "UserAccessToken not fetched yet");
			return response;
		}
		JSONObject result = playerService.getUserPlayList(
				spotifyUserAuthorizationCode.getTokenType() + " " + spotifyUserAuthorizationCode.getAccessToken(), user_id);
		System.out.println("Token Type: " + spotifyUserAuthorizationCode.getTokenType());
		System.out.println("Get Token : " + spotifyUserAuthorizationCode.getAccessToken());
		System.out.println("State : "+  spotifyUserAuthorizationCode.getState());
		return result;
	}
	
	
	@GetMapping("/getplaylistbyid/{playlist_id}")
	public JSONObject getPlayListByUser(@PathVariable String playlist_id) {
		
		System.out.println("into getplaylistbyid ");
		JSONObject response = new JSONObject();
		if (spotifyUserAuthorizationCode.getAccessToken() == null
				|| spotifyUserAuthorizationCode.getAccessToken().isEmpty()) {
			response.put("Error", "UserAccessToken not fetched yet");
			return response;
		}
		JSONObject result = playerService.getPlayListById(
				spotifyUserAuthorizationCode.getTokenType() + " " + spotifyUserAuthorizationCode.getAccessToken(), playlist_id);
		System.out.println("Token Type: " + spotifyUserAuthorizationCode.getTokenType());
		System.out.println("Get Token : " + spotifyUserAuthorizationCode.getAccessToken());
		return result;
	}
	
	
	
	
	

}
